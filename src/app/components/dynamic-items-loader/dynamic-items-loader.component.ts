import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ListItem } from 'src/app/models/ListItem';

@Component({
  selector: 'app-dynamic-items-loader',
  templateUrl: './dynamic-items-loader.component.html',
})
export class DynamicItemsLoaderComponent implements OnInit {
  @Input() listItems:[ListItem];
  listItemsToShow:[ListItem?];
  nItemsToShow:number = 0;
  avialableHeight:number = 0;
  pointer:number = 0;
  itemMarginHeight = 16;
  itemHeight:number = 54 + this.itemMarginHeight; //actualt height + margin
  lastScrollTop:number = 0;
  lastDirection:string = '';

  constructor() { }

  ngOnInit(): void {
    this.avialableHeight = window.innerHeight - 60;
    this.nItemsToShow = Math.floor(this.avialableHeight / this.itemHeight);
    const remainingHeight = this.avialableHeight - (this.nItemsToShow * 70);
    this.itemHeight += (remainingHeight / this.nItemsToShow) - this.itemMarginHeight;
    this.listItemsToShow = [];
    for (let i = this.pointer; i < this.nItemsToShow; i++) {
        this.listItemsToShow.push(this.listItems[i]);
    }
  }


  loadNextOrPrevItems(fetchNext:boolean) {
    this.pointer += this.nItemsToShow * (fetchNext ? 1 : -1);
    if (this.pointer < 0) {
        this.pointer = 0;
    }
    if (this.pointer > this.listItems.length - this.nItemsToShow) {
      this.pointer = this.listItems.length - this.nItemsToShow;
    }
    const listItemsToShow: [ListItem?] = [];
    for (let i = this.pointer; i < this.pointer + this.nItemsToShow; i++) {
        listItemsToShow.push(this.listItems[i]);
    }
    this.listItemsToShow = listItemsToShow;
  }
}
