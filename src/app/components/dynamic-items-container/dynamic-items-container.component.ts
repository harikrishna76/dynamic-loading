import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ListItem } from 'src/app/models/ListItem';

@Component({
  selector: 'app-dynamic-items-container',
  templateUrl: './dynamic-items-container.component.html',
  styleUrls: ['./dynamic-items-container.component.css']
})

export class DynamicItemsContainerComponent implements OnInit {
  @Input() listItemsToShow:[ListItem];
  @Input() avialableHeight:number;
  @Input() itemHeight:number;
  @Output() loadNextOrPrevItems: EventEmitter<boolean> = new EventEmitter();
  lastDirection:string = '';

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    setTimeout(() => {
      this.lastDirection = '';
    }, 1500)
  }

  handleOnScroll(event: any) {
    let dir = '';
    if (event.deltaY < 0) {
      dir = "up";
    } else if (event.deltaY > 0) {
      dir = "down";
    }
    if (this.lastDirection !== dir) {
      this.lastDirection = dir;
      this.loadNextOrPrevItems.emit(dir === 'down');
    }
  }
}
