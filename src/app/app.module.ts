import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DynamicItemsLoaderComponent } from './components/dynamic-items-loader/dynamic-items-loader.component';
import { LandingComponent } from './pages/landing/landing.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { DynamicItemsContainerComponent } from './components/dynamic-items-container/dynamic-items-container.component';

@NgModule({
  declarations: [
    AppComponent,
    DynamicItemsLoaderComponent,
    LandingComponent,
    ListItemComponent,
    DynamicItemsContainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
