import { Component, OnInit } from '@angular/core';
import { ListItem } from 'src/app/models/ListItem';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  listItems:ListItem[];
  lastScrollTop:number = 0;
  lastDirection:string = '';

  constructor() { }

  ngOnInit(): void {
    this.listItems = [];
    for (let id = 1; id <= 1500; id++) {
      this.listItems.push({ id, name: `List Item ${id}`});
    }
  }
}
